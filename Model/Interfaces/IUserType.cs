﻿namespace Gestin.Interfaces
{
    public interface IUserType
    {
        public string getUserRoleType();
        public string getUserFullName();
        public string getUserEmail();
        public string getUserRoleAndName();
    }
}
