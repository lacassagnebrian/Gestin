﻿using Gestin.UI.Custom;

namespace Gestin.UI.Home.Enrolments
{
    partial class formCareerEnrolmentStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formCareerEnrolmentStatistics));
            label2 = new Label();
            label1 = new Label();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            label7 = new Label();
            LblTotalEnroments = new Label();
            LblTotalMales = new Label();
            LblTotalFemales = new Label();
            LblAcademicallyBehind = new Label();
            LblNoAcademicDelay = new Label();
            LbLSubjectRecurring = new Label();
            LblFreeEnrolment = new Label();
            groupBox1 = new GroupBox();
            groupBox4 = new GroupBox();
            toggleButton1 = new ToggleButton();
            LblCourseFilter = new Label();
            CoursePnl = new Panel();
            cbbCourse = new ComboBox();
            lblCourse = new Label();
            moreStatisticsPnl = new Panel();
            label8 = new Label();
            LblPresentialEnrolment = new Label();
            lblCareerName = new Label();
            groupBox3 = new GroupBox();
            toggleSubjects = new ToggleButton();
            label12 = new Label();
            cbbCareerSelector = new ComboBox();
            lblTotalOtherGender = new Label();
            label11 = new Label();
            cbbSubjects = new ComboBox();
            materia = new Label();
            LblYear = new Label();
            label10 = new Label();
            groupBox2 = new GroupBox();
            btnSearchByYear = new Button();
            txtSearchYear = new TextBox();
            toggSearchByYear = new ToggleButton();
            LblSearch = new Label();
            lblCarrer = new Label();
            btnPromedios = new Button();
            groupBox1.SuspendLayout();
            groupBox4.SuspendLayout();
            CoursePnl.SuspendLayout();
            moreStatisticsPnl.SuspendLayout();
            groupBox3.SuspendLayout();
            groupBox2.SuspendLayout();
            SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label2.ForeColor = Color.White;
            label2.Location = new Point(17, 323);
            label2.Name = "label2";
            label2.Padding = new Padding(3);
            label2.Size = new Size(137, 27);
            label2.TabIndex = 5;
            label2.Text = "Matricula total :";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label1.ForeColor = Color.White;
            label1.Location = new Point(315, 323);
            label1.Name = "label1";
            label1.Padding = new Padding(3);
            label1.Size = new Size(85, 27);
            label1.TabIndex = 6;
            label1.Text = "Varones :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label3.ForeColor = Color.White;
            label3.Location = new Point(501, 323);
            label3.Name = "label3";
            label3.Padding = new Padding(3);
            label3.Size = new Size(85, 27);
            label3.TabIndex = 7;
            label3.Text = "Mujeres :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label4.ForeColor = Color.White;
            label4.Location = new Point(356, 8);
            label4.Name = "label4";
            label4.Padding = new Padding(3);
            label4.Size = new Size(192, 27);
            label4.TabIndex = 8;
            label4.Text = "Con atraso académico :";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label5.ForeColor = Color.White;
            label5.Location = new Point(3, 60);
            label5.Name = "label5";
            label5.Padding = new Padding(3);
            label5.Size = new Size(115, 27);
            label5.TabIndex = 9;
            label5.Text = "Recursantes :";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label6.ForeColor = Color.White;
            label6.Location = new Point(274, 60);
            label6.Name = "label6";
            label6.Padding = new Padding(3);
            label6.Size = new Size(69, 27);
            label6.TabIndex = 10;
            label6.Text = "Libres :";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label7.ForeColor = Color.White;
            label7.Location = new Point(3, 8);
            label7.Name = "label7";
            label7.Padding = new Padding(3);
            label7.Size = new Size(186, 27);
            label7.TabIndex = 11;
            label7.Text = "Sin atraso académico :";
            // 
            // LblTotalEnroments
            // 
            LblTotalEnroments.AutoSize = true;
            LblTotalEnroments.Font = new Font("Segoe UI", 12F);
            LblTotalEnroments.ForeColor = Color.FromArgb(255, 152, 0);
            LblTotalEnroments.Location = new Point(160, 323);
            LblTotalEnroments.Name = "LblTotalEnroments";
            LblTotalEnroments.Padding = new Padding(3);
            LblTotalEnroments.Size = new Size(127, 27);
            LblTotalEnroments.TabIndex = 12;
            LblTotalEnroments.Text = "TotalEnrolments";
            LblTotalEnroments.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // LblTotalMales
            // 
            LblTotalMales.AutoSize = true;
            LblTotalMales.Font = new Font("Segoe UI", 12F);
            LblTotalMales.ForeColor = Color.FromArgb(255, 152, 0);
            LblTotalMales.Location = new Point(406, 323);
            LblTotalMales.Name = "LblTotalMales";
            LblTotalMales.Padding = new Padding(3);
            LblTotalMales.Size = new Size(89, 27);
            LblTotalMales.TabIndex = 13;
            LblTotalMales.Text = "TotalMales";
            LblTotalMales.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // LblTotalFemales
            // 
            LblTotalFemales.AutoSize = true;
            LblTotalFemales.Font = new Font("Segoe UI", 12F);
            LblTotalFemales.ForeColor = Color.FromArgb(255, 152, 0);
            LblTotalFemales.Location = new Point(590, 323);
            LblTotalFemales.Name = "LblTotalFemales";
            LblTotalFemales.Padding = new Padding(3);
            LblTotalFemales.Size = new Size(105, 27);
            LblTotalFemales.TabIndex = 14;
            LblTotalFemales.Text = "TotalFemales";
            LblTotalFemales.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // LblAcademicallyBehind
            // 
            LblAcademicallyBehind.AutoSize = true;
            LblAcademicallyBehind.Font = new Font("Segoe UI", 12F);
            LblAcademicallyBehind.ForeColor = Color.FromArgb(255, 152, 0);
            LblAcademicallyBehind.Location = new Point(554, 8);
            LblAcademicallyBehind.Name = "LblAcademicallyBehind";
            LblAcademicallyBehind.Padding = new Padding(3);
            LblAcademicallyBehind.Size = new Size(155, 27);
            LblAcademicallyBehind.TabIndex = 15;
            LblAcademicallyBehind.Text = "AcademicallyBehind";
            LblAcademicallyBehind.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // LblNoAcademicDelay
            // 
            LblNoAcademicDelay.AutoSize = true;
            LblNoAcademicDelay.Font = new Font("Segoe UI", 12F);
            LblNoAcademicDelay.ForeColor = Color.FromArgb(255, 152, 0);
            LblNoAcademicDelay.Location = new Point(195, 8);
            LblNoAcademicDelay.Name = "LblNoAcademicDelay";
            LblNoAcademicDelay.Padding = new Padding(3);
            LblNoAcademicDelay.Size = new Size(143, 27);
            LblNoAcademicDelay.TabIndex = 16;
            LblNoAcademicDelay.Text = "NoAcademicDelay";
            LblNoAcademicDelay.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // LbLSubjectRecurring
            // 
            LbLSubjectRecurring.AutoSize = true;
            LbLSubjectRecurring.Font = new Font("Segoe UI", 12F);
            LbLSubjectRecurring.ForeColor = Color.FromArgb(255, 152, 0);
            LbLSubjectRecurring.Location = new Point(124, 60);
            LbLSubjectRecurring.Name = "LbLSubjectRecurring";
            LbLSubjectRecurring.Padding = new Padding(3);
            LbLSubjectRecurring.Size = new Size(135, 27);
            LbLSubjectRecurring.TabIndex = 17;
            LbLSubjectRecurring.Text = "SubjectRecurring";
            LbLSubjectRecurring.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // LblFreeEnrolment
            // 
            LblFreeEnrolment.AutoSize = true;
            LblFreeEnrolment.Font = new Font("Segoe UI", 12F);
            LblFreeEnrolment.ForeColor = Color.FromArgb(255, 152, 0);
            LblFreeEnrolment.Location = new Point(354, 60);
            LblFreeEnrolment.Name = "LblFreeEnrolment";
            LblFreeEnrolment.Padding = new Padding(3);
            LblFreeEnrolment.Size = new Size(118, 27);
            LblFreeEnrolment.TabIndex = 18;
            LblFreeEnrolment.Text = "FreeEnrolment";
            LblFreeEnrolment.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(groupBox4);
            groupBox1.Controls.Add(CoursePnl);
            groupBox1.Controls.Add(moreStatisticsPnl);
            groupBox1.Controls.Add(lblCareerName);
            groupBox1.Controls.Add(groupBox3);
            groupBox1.Controls.Add(cbbCareerSelector);
            groupBox1.Controls.Add(lblTotalOtherGender);
            groupBox1.Controls.Add(label11);
            groupBox1.Controls.Add(cbbSubjects);
            groupBox1.Controls.Add(materia);
            groupBox1.Controls.Add(LblYear);
            groupBox1.Controls.Add(label10);
            groupBox1.Controls.Add(groupBox2);
            groupBox1.Controls.Add(lblCarrer);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(LblTotalFemales);
            groupBox1.Controls.Add(LblTotalMales);
            groupBox1.Controls.Add(LblTotalEnroments);
            groupBox1.ForeColor = Color.WhiteSmoke;
            groupBox1.Location = new Point(12, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(1348, 525);
            groupBox1.TabIndex = 19;
            groupBox1.TabStop = false;
            groupBox1.Text = "Datos de matrícula";
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(toggleButton1);
            groupBox4.Controls.Add(LblCourseFilter);
            groupBox4.ForeColor = Color.White;
            groupBox4.Location = new Point(1059, 107);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(267, 66);
            groupBox4.TabIndex = 68;
            groupBox4.TabStop = false;
            groupBox4.Text = "Búsqueda por Curso";
            groupBox4.Visible = false;
            // 
            // toggleButton1
            // 
            toggleButton1.AutoSize = true;
            toggleButton1.FlatStyle = FlatStyle.Flat;
            toggleButton1.Location = new Point(182, 27);
            toggleButton1.MinimumSize = new Size(45, 22);
            toggleButton1.Name = "toggleButton1";
            toggleButton1.OffBackColor = Color.Gray;
            toggleButton1.OffToggleColor = Color.Gainsboro;
            toggleButton1.OnBackColor = Color.FromArgb(0, 150, 136);
            toggleButton1.OnToggleColor = Color.WhiteSmoke;
            toggleButton1.Size = new Size(45, 22);
            toggleButton1.TabIndex = 67;
            toggleButton1.UseVisualStyleBackColor = true;
            toggleButton1.CheckedChanged += toggleButton1_CheckedChanged;
            // 
            // LblCourseFilter
            // 
            LblCourseFilter.AutoSize = true;
            LblCourseFilter.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            LblCourseFilter.Location = new Point(20, 27);
            LblCourseFilter.Name = "LblCourseFilter";
            LblCourseFilter.Size = new Size(133, 21);
            LblCourseFilter.TabIndex = 0;
            LblCourseFilter.Text = "Todos los cursos";
            // 
            // CoursePnl
            // 
            CoursePnl.Controls.Add(cbbCourse);
            CoursePnl.Controls.Add(lblCourse);
            CoursePnl.Location = new Point(20, 180);
            CoursePnl.Name = "CoursePnl";
            CoursePnl.Size = new Size(223, 33);
            CoursePnl.TabIndex = 69;
            CoursePnl.Visible = false;
            // 
            // cbbCourse
            // 
            cbbCourse.BackColor = Color.FromArgb(44, 47, 51);
            cbbCourse.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbCourse.FlatStyle = FlatStyle.Flat;
            cbbCourse.Font = new Font("Segoe UI", 10F);
            cbbCourse.ForeColor = Color.White;
            cbbCourse.FormattingEnabled = true;
            cbbCourse.Items.AddRange(new object[] { "1ro", "2do", "3ro" });
            cbbCourse.Location = new Point(84, 3);
            cbbCourse.Name = "cbbCourse";
            cbbCourse.Size = new Size(131, 25);
            cbbCourse.TabIndex = 62;
            cbbCourse.SelectedIndexChanged += cbbCourse_SelectedIndexChanged;
            // 
            // lblCourse
            // 
            lblCourse.AutoSize = true;
            lblCourse.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            lblCourse.Location = new Point(3, 3);
            lblCourse.Name = "lblCourse";
            lblCourse.Size = new Size(57, 21);
            lblCourse.TabIndex = 57;
            lblCourse.Text = "Curso:";
            // 
            // moreStatisticsPnl
            // 
            moreStatisticsPnl.Controls.Add(label7);
            moreStatisticsPnl.Controls.Add(label6);
            moreStatisticsPnl.Controls.Add(label5);
            moreStatisticsPnl.Controls.Add(LblAcademicallyBehind);
            moreStatisticsPnl.Controls.Add(label4);
            moreStatisticsPnl.Controls.Add(LblNoAcademicDelay);
            moreStatisticsPnl.Controls.Add(LbLSubjectRecurring);
            moreStatisticsPnl.Controls.Add(LblFreeEnrolment);
            moreStatisticsPnl.Controls.Add(label8);
            moreStatisticsPnl.Controls.Add(LblPresentialEnrolment);
            moreStatisticsPnl.Location = new Point(17, 384);
            moreStatisticsPnl.Name = "moreStatisticsPnl";
            moreStatisticsPnl.Size = new Size(745, 108);
            moreStatisticsPnl.TabIndex = 68;
            moreStatisticsPnl.Visible = false;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label8.Location = new Point(478, 63);
            label8.Name = "label8";
            label8.Size = new Size(108, 21);
            label8.TabIndex = 55;
            label8.Text = "Presenciales:";
            // 
            // LblPresentialEnrolment
            // 
            LblPresentialEnrolment.AutoSize = true;
            LblPresentialEnrolment.Font = new Font("Segoe UI", 12F);
            LblPresentialEnrolment.ForeColor = Color.FromArgb(255, 152, 0);
            LblPresentialEnrolment.Location = new Point(586, 63);
            LblPresentialEnrolment.Name = "LblPresentialEnrolment";
            LblPresentialEnrolment.Size = new Size(150, 21);
            LblPresentialEnrolment.TabIndex = 56;
            LblPresentialEnrolment.Text = "PresentialEnrolment";
            // 
            // lblCareerName
            // 
            lblCareerName.AutoSize = true;
            lblCareerName.Font = new Font("Segoe UI", 12F);
            lblCareerName.ForeColor = Color.FromArgb(255, 152, 0);
            lblCareerName.Location = new Point(92, 29);
            lblCareerName.Name = "lblCareerName";
            lblCareerName.Padding = new Padding(3);
            lblCareerName.Size = new Size(62, 27);
            lblCareerName.TabIndex = 67;
            lblCareerName.Text = "Career";
            lblCareerName.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(toggleSubjects);
            groupBox3.Controls.Add(label12);
            groupBox3.ForeColor = Color.White;
            groupBox3.Location = new Point(786, 107);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(267, 66);
            groupBox3.TabIndex = 60;
            groupBox3.TabStop = false;
            groupBox3.Text = "Búsqueda por Materia";
            // 
            // toggleSubjects
            // 
            toggleSubjects.AutoSize = true;
            toggleSubjects.FlatStyle = FlatStyle.Flat;
            toggleSubjects.Location = new Point(182, 27);
            toggleSubjects.MinimumSize = new Size(45, 22);
            toggleSubjects.Name = "toggleSubjects";
            toggleSubjects.OffBackColor = Color.Gray;
            toggleSubjects.OffToggleColor = Color.Gainsboro;
            toggleSubjects.OnBackColor = Color.FromArgb(0, 150, 136);
            toggleSubjects.OnToggleColor = Color.WhiteSmoke;
            toggleSubjects.Size = new Size(45, 22);
            toggleSubjects.TabIndex = 67;
            toggleSubjects.UseVisualStyleBackColor = true;
            toggleSubjects.CheckedChanged += toggleSubjects_CheckedChanged;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label12.Location = new Point(20, 27);
            label12.Name = "label12";
            label12.Size = new Size(149, 21);
            label12.TabIndex = 0;
            label12.Text = "Todas las materias";
            // 
            // cbbCareerSelector
            // 
            cbbCareerSelector.BackColor = Color.FromArgb(44, 47, 51);
            cbbCareerSelector.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbCareerSelector.FlatStyle = FlatStyle.Flat;
            cbbCareerSelector.Font = new Font("Segoe UI", 10F);
            cbbCareerSelector.ForeColor = Color.White;
            cbbCareerSelector.FormattingEnabled = true;
            cbbCareerSelector.Location = new Point(432, 22);
            cbbCareerSelector.Margin = new Padding(3, 2, 3, 2);
            cbbCareerSelector.Name = "cbbCareerSelector";
            cbbCareerSelector.Size = new Size(53, 25);
            cbbCareerSelector.TabIndex = 30;
            cbbCareerSelector.Visible = false;
            // 
            // lblTotalOtherGender
            // 
            lblTotalOtherGender.AutoSize = true;
            lblTotalOtherGender.Font = new Font("Segoe UI", 12F);
            lblTotalOtherGender.ForeColor = Color.FromArgb(255, 152, 0);
            lblTotalOtherGender.Location = new Point(765, 323);
            lblTotalOtherGender.Name = "lblTotalOtherGender";
            lblTotalOtherGender.Padding = new Padding(3);
            lblTotalOtherGender.Size = new Size(88, 27);
            lblTotalOtherGender.TabIndex = 66;
            lblTotalOtherGender.Text = "TotalOther";
            lblTotalOtherGender.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label11.ForeColor = Color.White;
            label11.Location = new Point(701, 323);
            label11.Name = "label11";
            label11.Padding = new Padding(3);
            label11.Size = new Size(58, 27);
            label11.TabIndex = 65;
            label11.Text = "Otro :";
            // 
            // cbbSubjects
            // 
            cbbSubjects.BackColor = Color.FromArgb(44, 47, 51);
            cbbSubjects.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbSubjects.Enabled = false;
            cbbSubjects.FlatStyle = FlatStyle.Flat;
            cbbSubjects.Font = new Font("Segoe UI", 10F);
            cbbSubjects.ForeColor = Color.White;
            cbbSubjects.FormattingEnabled = true;
            cbbSubjects.Location = new Point(104, 133);
            cbbSubjects.Margin = new Padding(3, 2, 3, 2);
            cbbSubjects.Name = "cbbSubjects";
            cbbSubjects.Size = new Size(518, 25);
            cbbSubjects.TabIndex = 64;
            cbbSubjects.Visible = false;
            cbbSubjects.SelectedIndexChanged += cbbSubjects_SelectedIndexChanged;
            // 
            // materia
            // 
            materia.AutoSize = true;
            materia.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            materia.Location = new Point(17, 133);
            materia.Name = "materia";
            materia.Size = new Size(73, 21);
            materia.TabIndex = 63;
            materia.Text = "Materia:";
            materia.Visible = false;
            // 
            // LblYear
            // 
            LblYear.AutoSize = true;
            LblYear.Font = new Font("Segoe UI", 12F);
            LblYear.ForeColor = Color.FromArgb(255, 152, 0);
            LblYear.Location = new Point(71, 78);
            LblYear.Name = "LblYear";
            LblYear.Size = new Size(40, 21);
            LblYear.TabIndex = 61;
            LblYear.Text = "Year";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            label10.Location = new Point(17, 78);
            label10.Name = "label10";
            label10.Size = new Size(45, 21);
            label10.TabIndex = 60;
            label10.Text = "Año:";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(btnSearchByYear);
            groupBox2.Controls.Add(txtSearchYear);
            groupBox2.Controls.Add(toggSearchByYear);
            groupBox2.Controls.Add(LblSearch);
            groupBox2.ForeColor = Color.White;
            groupBox2.Location = new Point(786, 22);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(540, 66);
            groupBox2.TabIndex = 59;
            groupBox2.TabStop = false;
            groupBox2.Text = "Búsqueda por año";
            // 
            // btnSearchByYear
            // 
            btnSearchByYear.BackColor = Color.FromArgb(0, 150, 136);
            btnSearchByYear.Enabled = false;
            btnSearchByYear.FlatAppearance.BorderSize = 0;
            btnSearchByYear.FlatStyle = FlatStyle.Flat;
            btnSearchByYear.Font = new Font("Microsoft Sans Serif", 12F);
            btnSearchByYear.ForeColor = Color.White;
            btnSearchByYear.Location = new Point(439, 21);
            btnSearchByYear.Margin = new Padding(3, 2, 3, 2);
            btnSearchByYear.Name = "btnSearchByYear";
            btnSearchByYear.Size = new Size(83, 30);
            btnSearchByYear.TabIndex = 56;
            btnSearchByYear.Text = "Buscar";
            btnSearchByYear.UseVisualStyleBackColor = false;
            btnSearchByYear.Click += btnSearchByYear_Click;
            // 
            // txtSearchYear
            // 
            txtSearchYear.BackColor = Color.FromArgb(44, 47, 51);
            txtSearchYear.BorderStyle = BorderStyle.FixedSingle;
            txtSearchYear.Enabled = false;
            txtSearchYear.Font = new Font("Segoe UI", 12F);
            txtSearchYear.ForeColor = Color.White;
            txtSearchYear.Location = new Point(243, 22);
            txtSearchYear.Margin = new Padding(2, 1, 2, 1);
            txtSearchYear.Name = "txtSearchYear";
            txtSearchYear.PlaceholderText = "  ingrese un año";
            txtSearchYear.Size = new Size(165, 29);
            txtSearchYear.TabIndex = 55;
            // 
            // toggSearchByYear
            // 
            toggSearchByYear.AutoSize = true;
            toggSearchByYear.FlatStyle = FlatStyle.Flat;
            toggSearchByYear.Location = new Point(157, 27);
            toggSearchByYear.MinimumSize = new Size(45, 22);
            toggSearchByYear.Name = "toggSearchByYear";
            toggSearchByYear.OffBackColor = Color.Gray;
            toggSearchByYear.OffToggleColor = Color.Gainsboro;
            toggSearchByYear.OnBackColor = Color.FromArgb(0, 150, 136);
            toggSearchByYear.OnToggleColor = Color.WhiteSmoke;
            toggSearchByYear.Size = new Size(45, 22);
            toggSearchByYear.TabIndex = 54;
            toggSearchByYear.UseVisualStyleBackColor = true;
            toggSearchByYear.CheckedChanged += toggSearchByYear_CheckedChanged_1;
            // 
            // LblSearch
            // 
            LblSearch.AutoSize = true;
            LblSearch.Font = new Font("Segoe UI", 12F, FontStyle.Bold);
            LblSearch.Location = new Point(18, 26);
            LblSearch.Name = "LblSearch";
            LblSearch.Size = new Size(121, 21);
            LblSearch.TabIndex = 0;
            LblSearch.Text = "Todos los años";
            // 
            // lblCarrer
            // 
            lblCarrer.AutoSize = true;
            lblCarrer.Font = new Font("Segoe UI", 11.25F, FontStyle.Bold);
            lblCarrer.ForeColor = Color.WhiteSmoke;
            lblCarrer.Location = new Point(17, 33);
            lblCarrer.Name = "lblCarrer";
            lblCarrer.Size = new Size(68, 20);
            lblCarrer.TabIndex = 31;
            lblCarrer.Text = "Carrera :";
            // 
            // btnPromedios
            // 
            btnPromedios.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnPromedios.BackColor = Color.FromArgb(0, 150, 136);
            btnPromedios.FlatAppearance.BorderSize = 0;
            btnPromedios.FlatStyle = FlatStyle.Flat;
            btnPromedios.Font = new Font("Segoe UI", 12F);
            btnPromedios.ForeColor = Color.White;
            btnPromedios.Location = new Point(12, 546);
            btnPromedios.Margin = new Padding(3, 2, 3, 2);
            btnPromedios.Name = "btnPromedios";
            btnPromedios.Size = new Size(104, 28);
            btnPromedios.TabIndex = 42;
            btnPromedios.Text = "Promedios";
            btnPromedios.UseVisualStyleBackColor = false;
            // 
            // formCareerEnrolmentStatistics
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(44, 47, 51);
            ClientSize = new Size(1374, 585);
            Controls.Add(btnPromedios);
            Controls.Add(groupBox1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "formCareerEnrolmentStatistics";
            Text = "Gestin - Matricula por Carrera";
            Load += formCareerEnrolmentStatistics_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            CoursePnl.ResumeLayout(false);
            CoursePnl.PerformLayout();
            moreStatisticsPnl.ResumeLayout(false);
            moreStatisticsPnl.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Label label2;
        private Label label1;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label LblTotalEnroments;
        private Label LblTotalMales;
        private Label LblTotalFemales;
        private Label LblAcademicallyBehind;
        private Label LblNoAcademicDelay;
        private Label LbLSubjectRecurring;
        private Label LblFreeEnrolment;
        private GroupBox groupBox1;
        private Label lblCarrer;
        private ToggleButton toggSearchByAllCarrers;
        private Button btnPromedios;
        private Label label8;
        private Label LblPresentialEnrolment;
        private Label lblCourse;
        private GroupBox groupBox2;
        private Label LblSearch;
        private ToggleButton toggSearchByYear;
        private TextBox txtSearchYear;
        private Button btnSearchByYear;
        private Label LblYear;
        private Label label10;
        private ComboBox cbbCourse;
        private Label materia;
        private ComboBox cbbSubjects;
        private Label lblTotalOtherGender;
        private Label label11;
        private GroupBox groupBox3;
        private Label label12;
        private ComboBox cbbCareerSelector;
        private Label lblCareerName;
        private Panel moreStatisticsPnl;
        private Panel CoursePnl;
        private ToggleButton toggleSubjects;
        private GroupBox groupBox4;
        private ToggleButton toggleButton1;
        private Label LblCourseFilter;
    }
}