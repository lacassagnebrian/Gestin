﻿namespace Gestin.Deprecated
{
    partial class formStudentEnroment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formStudentEnroment));
            panel1 = new Panel();
            pictureBox1 = new PictureBox();
            pictureBox2 = new PictureBox();
            btnMinimize = new PictureBox();
            btnClose = new PictureBox();
            label14 = new Label();
            tableLayoutPanel1 = new TableLayoutPanel();
            btnViewConfirmPass = new PictureBox();
            txtConfirPassword = new TextBox();
            label13 = new Label();
            btnViewPass = new PictureBox();
            txtPassword = new TextBox();
            txtCelularDeEmergencia = new TextBox();
            label1 = new Label();
            cbSexo = new ComboBox();
            label8 = new Label();
            txtEmail = new TextBox();
            txtCelular = new TextBox();
            label9 = new Label();
            label16 = new Label();
            label6 = new Label();
            label5 = new Label();
            label4 = new Label();
            label3 = new Label();
            txtLugarDeNacimiento = new TextBox();
            txtDni = new TextBox();
            txtApellido = new TextBox();
            txtNombre = new TextBox();
            label2 = new Label();
            label7 = new Label();
            tableLayoutPanel3 = new TableLayoutPanel();
            radioButton3 = new RadioButton();
            radioButton4 = new RadioButton();
            panel2 = new Panel();
            lblError = new Label();
            toolTipFechaDeNacimiento = new ToolTip(components);
            btnRegistrarse = new Button();
            btnCancelar = new Button();
            txtFechaDeNacimiento = new TextBox();
            picInfo = new PictureBox();
            txtHorarioLaboral = new TextBox();
            label15 = new Label();
            txtActividadLaboral = new TextBox();
            label12 = new Label();
            label10 = new Label();
            cbTrabaja = new ComboBox();
            txtObraSocial = new TextBox();
            label11 = new Label();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)btnMinimize).BeginInit();
            ((System.ComponentModel.ISupportInitialize)btnClose).BeginInit();
            tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)btnViewConfirmPass).BeginInit();
            ((System.ComponentModel.ISupportInitialize)btnViewPass).BeginInit();
            tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)picInfo).BeginInit();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.BackColor = Color.FromArgb(114, 137, 218);
            panel1.Controls.Add(pictureBox1);
            panel1.Controls.Add(pictureBox2);
            panel1.Controls.Add(btnMinimize);
            panel1.Controls.Add(btnClose);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new Point(0, 0);
            panel1.Margin = new Padding(3, 4, 3, 4);
            panel1.Name = "panel1";
            panel1.Size = new Size(1201, 39);
            panel1.TabIndex = 3;
            panel1.MouseDown += panel1_MouseDown;
            // 
            // pictureBox1
            // 
            pictureBox1.Anchor = AnchorStyles.Right;
            pictureBox1.Cursor = Cursors.Hand;
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(1143, 4);
            pictureBox1.Margin = new Padding(3, 4, 3, 4);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(23, 27);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 4;
            pictureBox1.TabStop = false;
            pictureBox1.Click += btnMinimize_Click;
            // 
            // pictureBox2
            // 
            pictureBox2.Anchor = AnchorStyles.Right;
            pictureBox2.Cursor = Cursors.Hand;
            pictureBox2.Image = (Image)resources.GetObject("pictureBox2.Image");
            pictureBox2.Location = new Point(1173, 4);
            pictureBox2.Margin = new Padding(3, 4, 3, 4);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(23, 27);
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.TabIndex = 3;
            pictureBox2.TabStop = false;
            pictureBox2.Click += btnClose_Click;
            // 
            // btnMinimize
            // 
            btnMinimize.Anchor = AnchorStyles.Right;
            btnMinimize.Cursor = Cursors.Hand;
            btnMinimize.Image = (Image)resources.GetObject("btnMinimize.Image");
            btnMinimize.Location = new Point(1433, -44);
            btnMinimize.Margin = new Padding(3, 4, 3, 4);
            btnMinimize.Name = "btnMinimize";
            btnMinimize.Size = new Size(23, 27);
            btnMinimize.SizeMode = PictureBoxSizeMode.StretchImage;
            btnMinimize.TabIndex = 2;
            btnMinimize.TabStop = false;
            // 
            // btnClose
            // 
            btnClose.Anchor = AnchorStyles.Right;
            btnClose.Cursor = Cursors.Hand;
            btnClose.Image = (Image)resources.GetObject("btnClose.Image");
            btnClose.Location = new Point(1463, -44);
            btnClose.Margin = new Padding(3, 4, 3, 4);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(23, 27);
            btnClose.SizeMode = PictureBoxSizeMode.StretchImage;
            btnClose.TabIndex = 1;
            btnClose.TabStop = false;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Font = new Font("Microsoft Sans Serif", 26.25F, FontStyle.Regular, GraphicsUnit.Point);
            label14.ForeColor = Color.White;
            label14.Location = new Point(384, 43);
            label14.Name = "label14";
            label14.Size = new Size(358, 52);
            label14.TabIndex = 99;
            label14.Text = "Crear una cuenta";
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            tableLayoutPanel1.BackColor = Color.FromArgb(44, 47, 51);
            tableLayoutPanel1.ColumnCount = 5;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.Controls.Add(btnViewConfirmPass, 2, 2);
            tableLayoutPanel1.Controls.Add(txtConfirPassword, 1, 2);
            tableLayoutPanel1.Controls.Add(label13, 0, 2);
            tableLayoutPanel1.Controls.Add(btnViewPass, 2, 1);
            tableLayoutPanel1.Controls.Add(txtPassword, 1, 1);
            tableLayoutPanel1.Controls.Add(btnRegistrarse, 3, 7);
            tableLayoutPanel1.Controls.Add(btnCancelar, 4, 7);
            tableLayoutPanel1.Controls.Add(txtCelularDeEmergencia, 4, 2);
            tableLayoutPanel1.Controls.Add(label1, 0, 0);
            tableLayoutPanel1.Controls.Add(label10, 3, 4);
            tableLayoutPanel1.Controls.Add(cbTrabaja, 4, 4);
            tableLayoutPanel1.Controls.Add(txtObraSocial, 4, 3);
            tableLayoutPanel1.Controls.Add(cbSexo, 4, 0);
            tableLayoutPanel1.Controls.Add(label8, 3, 0);
            tableLayoutPanel1.Controls.Add(txtEmail, 1, 0);
            tableLayoutPanel1.Controls.Add(txtCelular, 4, 1);
            tableLayoutPanel1.Controls.Add(label9, 3, 1);
            tableLayoutPanel1.Controls.Add(label11, 3, 3);
            tableLayoutPanel1.Controls.Add(label16, 3, 2);
            tableLayoutPanel1.Controls.Add(txtHorarioLaboral, 4, 6);
            tableLayoutPanel1.Controls.Add(txtActividadLaboral, 4, 5);
            tableLayoutPanel1.Controls.Add(label15, 3, 6);
            tableLayoutPanel1.Controls.Add(label6, 0, 7);
            tableLayoutPanel1.Controls.Add(label5, 0, 5);
            tableLayoutPanel1.Controls.Add(label4, 0, 4);
            tableLayoutPanel1.Controls.Add(label3, 0, 3);
            tableLayoutPanel1.Controls.Add(txtFechaDeNacimiento, 1, 7);
            tableLayoutPanel1.Controls.Add(txtLugarDeNacimiento, 1, 6);
            tableLayoutPanel1.Controls.Add(txtDni, 1, 5);
            tableLayoutPanel1.Controls.Add(txtApellido, 1, 4);
            tableLayoutPanel1.Controls.Add(txtNombre, 1, 3);
            tableLayoutPanel1.Controls.Add(label2, 0, 1);
            tableLayoutPanel1.Controls.Add(picInfo, 2, 7);
            tableLayoutPanel1.Controls.Add(label12, 3, 5);
            tableLayoutPanel1.Controls.Add(label7, 0, 6);
            tableLayoutPanel1.Location = new Point(14, 104);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 7);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 8;
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.Size = new Size(1175, 527);
            tableLayoutPanel1.TabIndex = 18;
            // 
            // btnViewConfirmPass
            // 
            btnViewConfirmPass.BackColor = Color.FromArgb(44, 47, 51);
            btnViewConfirmPass.Cursor = Cursors.Hand;
            btnViewConfirmPass.Image = (Image)resources.GetObject("btnViewConfirmPass.Image");
            btnViewConfirmPass.Location = new Point(508, 136);
            btnViewConfirmPass.Margin = new Padding(3, 4, 3, 4);
            btnViewConfirmPass.Name = "btnViewConfirmPass";
            btnViewConfirmPass.Size = new Size(29, 33);
            btnViewConfirmPass.SizeMode = PictureBoxSizeMode.StretchImage;
            btnViewConfirmPass.TabIndex = 101;
            btnViewConfirmPass.TabStop = false;
            btnViewConfirmPass.Click += btnViewConfirmPass_Click;
            // 
            // txtConfirPassword
            // 
            txtConfirPassword.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtConfirPassword.BackColor = Color.FromArgb(35, 39, 42);
            txtConfirPassword.BorderStyle = BorderStyle.None;
            txtConfirPassword.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtConfirPassword.ForeColor = Color.White;
            txtConfirPassword.ImeMode = ImeMode.NoControl;
            txtConfirPassword.Location = new Point(251, 136);
            txtConfirPassword.Margin = new Padding(3, 4, 3, 27);
            txtConfirPassword.Multiline = true;
            txtConfirPassword.Name = "txtConfirPassword";
            txtConfirPassword.PasswordChar = '*';
            txtConfirPassword.Size = new Size(251, 33);
            txtConfirPassword.TabIndex = 3;
            txtConfirPassword.WordWrap = false;
            // 
            // label13
            // 
            label13.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label13.AutoSize = true;
            label13.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label13.ForeColor = Color.White;
            label13.Location = new Point(3, 138);
            label13.Margin = new Padding(3, 4, 3, 27);
            label13.Name = "label13";
            label13.Size = new Size(242, 29);
            label13.TabIndex = 100;
            label13.Text = "Confirmar contraseña";
            label13.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // btnViewPass
            // 
            btnViewPass.BackColor = Color.FromArgb(44, 47, 51);
            btnViewPass.Cursor = Cursors.Hand;
            btnViewPass.Image = (Image)resources.GetObject("btnViewPass.Image");
            btnViewPass.Location = new Point(508, 72);
            btnViewPass.Margin = new Padding(3, 4, 3, 4);
            btnViewPass.Name = "btnViewPass";
            btnViewPass.Size = new Size(29, 33);
            btnViewPass.SizeMode = PictureBoxSizeMode.StretchImage;
            btnViewPass.TabIndex = 100;
            btnViewPass.TabStop = false;
            btnViewPass.Click += btnViewPass_Click;
            // 
            // txtPassword
            // 
            txtPassword.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtPassword.BackColor = Color.FromArgb(35, 39, 42);
            txtPassword.BorderStyle = BorderStyle.None;
            txtPassword.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtPassword.ForeColor = Color.White;
            txtPassword.ImeMode = ImeMode.NoControl;
            txtPassword.Location = new Point(251, 72);
            txtPassword.Margin = new Padding(3, 4, 3, 27);
            txtPassword.Multiline = true;
            txtPassword.Name = "txtPassword";
            txtPassword.PasswordChar = '*';
            txtPassword.Size = new Size(251, 33);
            txtPassword.TabIndex = 2;
            txtPassword.WordWrap = false;
            // 
            // txtCelularDeEmergencia
            // 
            txtCelularDeEmergencia.BackColor = Color.FromArgb(35, 39, 42);
            txtCelularDeEmergencia.BorderStyle = BorderStyle.None;
            txtCelularDeEmergencia.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtCelularDeEmergencia.ForeColor = Color.White;
            txtCelularDeEmergencia.ImeMode = ImeMode.NoControl;
            txtCelularDeEmergencia.Location = new Point(839, 136);
            txtCelularDeEmergencia.Margin = new Padding(3, 4, 3, 27);
            txtCelularDeEmergencia.Multiline = true;
            txtCelularDeEmergencia.Name = "txtCelularDeEmergencia";
            txtCelularDeEmergencia.Size = new Size(251, 33);
            txtCelularDeEmergencia.TabIndex = 11;
            txtCelularDeEmergencia.WordWrap = false;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label1.ForeColor = Color.White;
            label1.ImageAlign = ContentAlignment.MiddleLeft;
            label1.Location = new Point(3, 8);
            label1.Margin = new Padding(3, 4, 3, 27);
            label1.Name = "label1";
            label1.Size = new Size(242, 29);
            label1.TabIndex = 0;
            label1.Text = "Email";
            label1.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // cbSexo
            // 
            cbSexo.BackColor = Color.FromArgb(35, 39, 42);
            cbSexo.DropDownStyle = ComboBoxStyle.DropDownList;
            cbSexo.FlatStyle = FlatStyle.Flat;
            cbSexo.ForeColor = Color.White;
            cbSexo.FormattingEnabled = true;
            cbSexo.Items.AddRange(new object[] { "Hombre", "Mujer" });
            cbSexo.Location = new Point(839, 13);
            cbSexo.Margin = new Padding(3, 13, 3, 27);
            cbSexo.Name = "cbSexo";
            cbSexo.Size = new Size(251, 28);
            cbSexo.TabIndex = 9;
            // 
            // label8
            // 
            label8.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label8.AutoSize = true;
            label8.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label8.ForeColor = Color.White;
            label8.Location = new Point(543, 8);
            label8.Margin = new Padding(3, 4, 0, 27);
            label8.Name = "label8";
            label8.Size = new Size(293, 29);
            label8.TabIndex = 23;
            label8.Text = "Sexo";
            label8.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // txtEmail
            // 
            txtEmail.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtEmail.BackColor = Color.FromArgb(35, 39, 42);
            txtEmail.BorderStyle = BorderStyle.None;
            txtEmail.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtEmail.ForeColor = Color.White;
            txtEmail.ImeMode = ImeMode.NoControl;
            txtEmail.Location = new Point(251, 6);
            txtEmail.Margin = new Padding(3, 4, 3, 27);
            txtEmail.Multiline = true;
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new Size(251, 33);
            txtEmail.TabIndex = 1;
            txtEmail.WordWrap = false;
            // 
            // txtCelular
            // 
            txtCelular.BackColor = Color.FromArgb(35, 39, 42);
            txtCelular.BorderStyle = BorderStyle.None;
            txtCelular.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtCelular.ForeColor = Color.White;
            txtCelular.ImeMode = ImeMode.NoControl;
            txtCelular.Location = new Point(839, 72);
            txtCelular.Margin = new Padding(3, 4, 3, 27);
            txtCelular.Multiline = true;
            txtCelular.Name = "txtCelular";
            txtCelular.Size = new Size(251, 33);
            txtCelular.TabIndex = 10;
            txtCelular.WordWrap = false;
            // 
            // label9
            // 
            label9.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label9.AutoSize = true;
            label9.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label9.ForeColor = Color.White;
            label9.Location = new Point(543, 74);
            label9.Margin = new Padding(3, 4, 0, 27);
            label9.Name = "label9";
            label9.Size = new Size(293, 29);
            label9.TabIndex = 16;
            label9.Text = "Celular";
            label9.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            label16.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label16.AutoSize = true;
            label16.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label16.ForeColor = Color.White;
            label16.Location = new Point(543, 138);
            label16.Margin = new Padding(3, 4, 0, 27);
            label16.Name = "label16";
            label16.Size = new Size(293, 29);
            label16.TabIndex = 22;
            label16.Text = "Celular de emergencia";
            label16.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            label6.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label6.AutoSize = true;
            label6.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label6.ForeColor = Color.White;
            label6.Location = new Point(3, 465);
            label6.Margin = new Padding(3, 4, 3, 27);
            label6.Name = "label6";
            label6.Size = new Size(242, 29);
            label6.TabIndex = 5;
            label6.Text = "Fecha de nacimiento";
            label6.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            label5.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label5.AutoSize = true;
            label5.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label5.ForeColor = Color.White;
            label5.Location = new Point(3, 334);
            label5.Margin = new Padding(3, 4, 3, 27);
            label5.Name = "label5";
            label5.Size = new Size(242, 29);
            label5.TabIndex = 4;
            label5.Text = "Dni";
            label5.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label4.AutoSize = true;
            label4.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label4.ForeColor = Color.White;
            label4.Location = new Point(3, 268);
            label4.Margin = new Padding(3, 4, 3, 27);
            label4.Name = "label4";
            label4.Size = new Size(242, 29);
            label4.TabIndex = 3;
            label4.Text = "Apellido";
            label4.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label3.AutoSize = true;
            label3.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label3.ForeColor = Color.White;
            label3.Location = new Point(3, 202);
            label3.Margin = new Padding(3, 4, 3, 27);
            label3.Name = "label3";
            label3.Size = new Size(242, 29);
            label3.TabIndex = 2;
            label3.Text = "Nombre";
            label3.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // txtLugarDeNacimiento
            // 
            txtLugarDeNacimiento.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtLugarDeNacimiento.BackColor = Color.FromArgb(35, 39, 42);
            txtLugarDeNacimiento.BorderStyle = BorderStyle.None;
            txtLugarDeNacimiento.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtLugarDeNacimiento.ForeColor = Color.White;
            txtLugarDeNacimiento.ImeMode = ImeMode.NoControl;
            txtLugarDeNacimiento.Location = new Point(251, 396);
            txtLugarDeNacimiento.Margin = new Padding(3, 4, 3, 27);
            txtLugarDeNacimiento.Multiline = true;
            txtLugarDeNacimiento.Name = "txtLugarDeNacimiento";
            txtLugarDeNacimiento.Size = new Size(251, 33);
            txtLugarDeNacimiento.TabIndex = 7;
            txtLugarDeNacimiento.WordWrap = false;
            // 
            // txtDni
            // 
            txtDni.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtDni.BackColor = Color.FromArgb(35, 39, 42);
            txtDni.BorderStyle = BorderStyle.None;
            txtDni.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtDni.ForeColor = Color.White;
            txtDni.ImeMode = ImeMode.NoControl;
            txtDni.Location = new Point(251, 332);
            txtDni.Margin = new Padding(3, 4, 3, 27);
            txtDni.Multiline = true;
            txtDni.Name = "txtDni";
            txtDni.Size = new Size(251, 33);
            txtDni.TabIndex = 6;
            txtDni.WordWrap = false;
            // 
            // txtApellido
            // 
            txtApellido.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtApellido.BackColor = Color.FromArgb(35, 39, 42);
            txtApellido.BorderStyle = BorderStyle.None;
            txtApellido.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtApellido.ForeColor = Color.White;
            txtApellido.ImeMode = ImeMode.NoControl;
            txtApellido.Location = new Point(251, 266);
            txtApellido.Margin = new Padding(3, 4, 3, 27);
            txtApellido.Multiline = true;
            txtApellido.Name = "txtApellido";
            txtApellido.Size = new Size(251, 33);
            txtApellido.TabIndex = 5;
            txtApellido.WordWrap = false;
            // 
            // txtNombre
            // 
            txtNombre.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtNombre.BackColor = Color.FromArgb(35, 39, 42);
            txtNombre.BorderStyle = BorderStyle.None;
            txtNombre.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtNombre.ForeColor = Color.White;
            txtNombre.ImeMode = ImeMode.NoControl;
            txtNombre.Location = new Point(251, 200);
            txtNombre.Margin = new Padding(3, 4, 3, 27);
            txtNombre.Multiline = true;
            txtNombre.Name = "txtNombre";
            txtNombre.Size = new Size(251, 33);
            txtNombre.TabIndex = 4;
            txtNombre.WordWrap = false;
            // 
            // label2
            // 
            label2.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label2.AutoSize = true;
            label2.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label2.ForeColor = Color.White;
            label2.Location = new Point(3, 74);
            label2.Margin = new Padding(3, 4, 3, 27);
            label2.Name = "label2";
            label2.Size = new Size(242, 29);
            label2.TabIndex = 99;
            label2.Text = "Contraseña";
            label2.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            label7.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label7.AutoSize = true;
            label7.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label7.ForeColor = Color.White;
            label7.Location = new Point(3, 398);
            label7.Margin = new Padding(3, 4, 3, 27);
            label7.Name = "label7";
            label7.Size = new Size(242, 29);
            label7.TabIndex = 6;
            label7.Text = "Lugar de nacimiento";
            label7.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 1;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel3.Controls.Add(radioButton3, 0, 0);
            tableLayoutPanel3.Location = new Point(0, 0);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel3.Size = new Size(200, 100);
            tableLayoutPanel3.TabIndex = 0;
            // 
            // radioButton3
            // 
            radioButton3.AutoSize = true;
            radioButton3.Font = new Font("Microsoft Sans Serif", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            radioButton3.ForeColor = Color.White;
            radioButton3.Location = new Point(3, 3);
            radioButton3.Name = "radioButton3";
            radioButton3.Size = new Size(100, 14);
            radioButton3.TabIndex = 0;
            radioButton3.TabStop = true;
            radioButton3.Text = "Hombre";
            radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            radioButton4.AutoSize = true;
            radioButton4.Font = new Font("Microsoft Sans Serif", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            radioButton4.ForeColor = Color.White;
            radioButton4.Location = new Point(3, 33);
            radioButton4.Name = "radioButton4";
            radioButton4.Size = new Size(69, 24);
            radioButton4.TabIndex = 1;
            radioButton4.TabStop = true;
            radioButton4.Text = "Mujer";
            radioButton4.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            panel2.BackColor = Color.FromArgb(114, 137, 218);
            panel2.Dock = DockStyle.Bottom;
            panel2.Location = new Point(0, 680);
            panel2.Margin = new Padding(3, 4, 3, 4);
            panel2.Name = "panel2";
            panel2.Size = new Size(1201, 24);
            panel2.TabIndex = 19;
            // 
            // lblError
            // 
            lblError.AutoSize = true;
            lblError.Font = new Font("Microsoft Sans Serif", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            lblError.ForeColor = Color.DarkGray;
            lblError.Image = (Image)resources.GetObject("lblError.Image");
            lblError.ImageAlign = ContentAlignment.MiddleLeft;
            lblError.Location = new Point(12, 638);
            lblError.Name = "lblError";
            lblError.Size = new Size(45, 24);
            lblError.TabIndex = 20;
            lblError.Text = "       ";
            lblError.Visible = false;
            // 
            // btnRegistrarse
            // 
            btnRegistrarse.BackColor = Color.FromArgb(114, 137, 218);
            btnRegistrarse.Cursor = Cursors.Hand;
            btnRegistrarse.FlatStyle = FlatStyle.Flat;
            btnRegistrarse.Font = new Font("Microsoft Sans Serif", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            btnRegistrarse.ForeColor = Color.Black;
            btnRegistrarse.Location = new Point(543, 460);
            btnRegistrarse.Margin = new Padding(3, 4, 3, 4);
            btnRegistrarse.Name = "btnRegistrarse";
            btnRegistrarse.Size = new Size(290, 57);
            btnRegistrarse.TabIndex = 16;
            btnRegistrarse.Text = "Registrarse";
            btnRegistrarse.UseVisualStyleBackColor = false;
            btnRegistrarse.Click += btnRegistrarse_Click;
            // 
            // btnCancelar
            // 
            btnCancelar.BackColor = Color.FromArgb(114, 137, 218);
            btnCancelar.Cursor = Cursors.Hand;
            btnCancelar.FlatStyle = FlatStyle.Flat;
            btnCancelar.Font = new Font("Microsoft Sans Serif", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            btnCancelar.ForeColor = Color.Black;
            btnCancelar.Location = new Point(841, 461);
            btnCancelar.Margin = new Padding(5);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Padding = new Padding(5);
            btnCancelar.Size = new Size(300, 56);
            btnCancelar.TabIndex = 17;
            btnCancelar.Text = "Cancelar";
            btnCancelar.UseVisualStyleBackColor = false;
            btnCancelar.Click += btnCancelar_Click;
            // 
            // txtFechaDeNacimiento
            // 
            txtFechaDeNacimiento.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            txtFechaDeNacimiento.BackColor = Color.FromArgb(35, 39, 42);
            txtFechaDeNacimiento.BorderStyle = BorderStyle.None;
            txtFechaDeNacimiento.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtFechaDeNacimiento.ForeColor = Color.White;
            txtFechaDeNacimiento.ImeMode = ImeMode.NoControl;
            txtFechaDeNacimiento.Location = new Point(251, 463);
            txtFechaDeNacimiento.Margin = new Padding(3, 4, 3, 27);
            txtFechaDeNacimiento.Multiline = true;
            txtFechaDeNacimiento.Name = "txtFechaDeNacimiento";
            txtFechaDeNacimiento.PlaceholderText = "dd/mm/aaaa";
            txtFechaDeNacimiento.Size = new Size(251, 33);
            txtFechaDeNacimiento.TabIndex = 8;
            txtFechaDeNacimiento.WordWrap = false;
            // 
            // picInfo
            // 
            picInfo.Cursor = Cursors.Hand;
            picInfo.Image = (Image)resources.GetObject("picInfo.Image");
            picInfo.Location = new Point(508, 460);
            picInfo.Margin = new Padding(3, 4, 3, 4);
            picInfo.Name = "picInfo";
            picInfo.Size = new Size(16, 16);
            picInfo.SizeMode = PictureBoxSizeMode.AutoSize;
            picInfo.TabIndex = 24;
            picInfo.TabStop = false;
            picInfo.MouseHover += picInfo_MouseHover;
            // 
            // txtHorarioLaboral
            // 
            txtHorarioLaboral.BackColor = Color.FromArgb(35, 39, 42);
            txtHorarioLaboral.BorderStyle = BorderStyle.None;
            txtHorarioLaboral.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtHorarioLaboral.ForeColor = Color.White;
            txtHorarioLaboral.ImeMode = ImeMode.NoControl;
            txtHorarioLaboral.Location = new Point(839, 396);
            txtHorarioLaboral.Margin = new Padding(3, 4, 3, 27);
            txtHorarioLaboral.Multiline = true;
            txtHorarioLaboral.Name = "txtHorarioLaboral";
            txtHorarioLaboral.Size = new Size(251, 33);
            txtHorarioLaboral.TabIndex = 15;
            txtHorarioLaboral.WordWrap = false;
            // 
            // label15
            // 
            label15.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label15.AutoSize = true;
            label15.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label15.ForeColor = Color.White;
            label15.Location = new Point(543, 398);
            label15.Margin = new Padding(3, 4, 0, 27);
            label15.Name = "label15";
            label15.Size = new Size(293, 29);
            label15.TabIndex = 21;
            label15.Text = "Horario laboral";
            label15.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // txtActividadLaboral
            // 
            txtActividadLaboral.BackColor = Color.FromArgb(35, 39, 42);
            txtActividadLaboral.BorderStyle = BorderStyle.None;
            txtActividadLaboral.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtActividadLaboral.ForeColor = Color.White;
            txtActividadLaboral.ImeMode = ImeMode.NoControl;
            txtActividadLaboral.Location = new Point(839, 332);
            txtActividadLaboral.Margin = new Padding(3, 4, 3, 27);
            txtActividadLaboral.Multiline = true;
            txtActividadLaboral.Name = "txtActividadLaboral";
            txtActividadLaboral.Size = new Size(251, 33);
            txtActividadLaboral.TabIndex = 14;
            txtActividadLaboral.WordWrap = false;
            // 
            // label12
            // 
            label12.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label12.AutoSize = true;
            label12.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label12.ForeColor = Color.White;
            label12.Location = new Point(543, 334);
            label12.Margin = new Padding(3, 4, 0, 27);
            label12.Name = "label12";
            label12.Size = new Size(293, 29);
            label12.TabIndex = 19;
            label12.Text = "Actividad laboral";
            label12.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            label10.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label10.AutoSize = true;
            label10.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label10.ForeColor = Color.White;
            label10.Location = new Point(543, 268);
            label10.Margin = new Padding(3, 4, 0, 27);
            label10.Name = "label10";
            label10.Size = new Size(293, 29);
            label10.TabIndex = 17;
            label10.Text = "¿Trabaja?";
            label10.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // cbTrabaja
            // 
            cbTrabaja.BackColor = Color.FromArgb(35, 39, 42);
            cbTrabaja.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTrabaja.FlatStyle = FlatStyle.Popup;
            cbTrabaja.ForeColor = Color.White;
            cbTrabaja.FormattingEnabled = true;
            cbTrabaja.Items.AddRange(new object[] { "Si", "No" });
            cbTrabaja.Location = new Point(839, 273);
            cbTrabaja.Margin = new Padding(3, 13, 3, 27);
            cbTrabaja.Name = "cbTrabaja";
            cbTrabaja.Size = new Size(251, 28);
            cbTrabaja.TabIndex = 13;
            // 
            // txtObraSocial
            // 
            txtObraSocial.BackColor = Color.FromArgb(35, 39, 42);
            txtObraSocial.BorderStyle = BorderStyle.None;
            txtObraSocial.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtObraSocial.ForeColor = Color.White;
            txtObraSocial.ImeMode = ImeMode.NoControl;
            txtObraSocial.Location = new Point(839, 200);
            txtObraSocial.Margin = new Padding(3, 4, 3, 27);
            txtObraSocial.Multiline = true;
            txtObraSocial.Name = "txtObraSocial";
            txtObraSocial.Size = new Size(251, 33);
            txtObraSocial.TabIndex = 12;
            txtObraSocial.WordWrap = false;
            // 
            // label11
            // 
            label11.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            label11.AutoSize = true;
            label11.Font = new Font("Microsoft Sans Serif", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            label11.ForeColor = Color.White;
            label11.Location = new Point(543, 202);
            label11.Margin = new Padding(3, 4, 0, 27);
            label11.Name = "label11";
            label11.Size = new Size(293, 29);
            label11.TabIndex = 18;
            label11.Text = "Obra social";
            label11.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // formStudentEnroment
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(35, 39, 40);
            ClientSize = new Size(1201, 704);
            Controls.Add(lblError);
            Controls.Add(panel2);
            Controls.Add(tableLayoutPanel1);
            Controls.Add(label14);
            Controls.Add(panel1);
            FormBorderStyle = FormBorderStyle.None;
            Margin = new Padding(3, 4, 3, 4);
            Name = "formStudentEnroment";
            Text = "formRegistroAlumno";
            Load += formRegistroAlumno_Load;
            MouseDown += formRegistroAlumno_MouseDown;
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ((System.ComponentModel.ISupportInitialize)btnMinimize).EndInit();
            ((System.ComponentModel.ISupportInitialize)btnClose).EndInit();
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)btnViewConfirmPass).EndInit();
            ((System.ComponentModel.ISupportInitialize)btnViewPass).EndInit();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)picInfo).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Panel panel1;
        private PictureBox btnMinimize;
        private PictureBox btnClose;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private Label label14;
        private TableLayoutPanel tableLayoutPanel1;
        private Label label1;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label9;
        private Label label16;
        private ComboBox cbSexo;
        private TableLayoutPanel tableLayoutPanel3;
        private RadioButton radioButton3;
        private RadioButton radioButton4;
        private Panel panel2;
        private TextBox txtEmail;
        private TextBox txtPassword;
        private TextBox txtNombre;
        private TextBox txtApellido;
        private TextBox txtDni;
        private TextBox txtLugarDeNacimiento;
        private TextBox txtCelular;
        private TextBox txtCelularDeEmergencia;
        private Label label8;
        private Label lblError;
        private ToolTip toolTipFechaDeNacimiento;
        private PictureBox btnViewPass;
        private PictureBox btnViewConfirmPass;
        private TextBox txtConfirPassword;
        private Label label13;
        private Button btnRegistrarse;
        private Button btnCancelar;
        private Label label10;
        private ComboBox cbTrabaja;
        private TextBox txtObraSocial;
        private Label label11;
        private TextBox txtHorarioLaboral;
        private TextBox txtActividadLaboral;
        private Label label15;
        private TextBox txtFechaDeNacimiento;
        private PictureBox picInfo;
        private Label label12;
    }
}